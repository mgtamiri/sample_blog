# Sample Blog Django Project

as a test project by **Moein Golesorkh**. This project is merely a sample that was completed in a short amount of time and is not appropriate for use in production.

## Features
- python 3.10
- last django LTS and django rest framework
- swagger base documentation
- sample unit tests and api tests
- use postgres SQL

## How to run
- ### Docker compose
```bash
export SECRET_KEY="django-insecure-(t^saoi1#vb%vfy6y97@(kh99uj!=j2o7t+$8o^wy#-itoplyw"
export DEBUG=TRUE
docker-compose up -d --build
```
- ### Local use
```bash
pip install -r requirements.txt
python manage.py migrate
python manage.py importdata
python manage.py runserver
```

## How to use
you can easily access project from **127.0.0.1:8002**

## Sample API call
```bash
curl --location 'http://127.0.0.1:8002/api/v1/posts/' \
--header 'Authorization: Bearer testtoken'
```

## Documentation
you can find documents base on swagger on route : **/swagger/**

## sample tests
you can run tests use **manage.py test** or from docker compose use :
```bash
docker-compose exec web python manage.py test
```
## sync data to API
A system based on events will sync all changes from the master to the remote API.
The syncdata custom command will apply events in the row one by one to the API.
This command must be executed during the times when the system requires synchronization.

```bash
docker-compose exec web python manage.py syncdata
```