from rest_framework.viewsets import ModelViewSet

from commons.custom_authentications import FakeBearerAuthentication
from commons.custom_permissions import FakeIsAuthenticated
from content.models import Post, Comment
from content.serializers import PostSerializer, CommentSerializer


class PostViewSet(ModelViewSet):
    """
    A simple ViewSet for listing or retrieving posts.
    """
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    authentication_classes = [FakeBearerAuthentication]
    permission_classes = [FakeIsAuthenticated]

    def perform_create(self, serializer):
        # we assume all new post users by this ID. this is not a standard approach.
        serializer.save(user_id=99999942)


class CommentViewSet(ModelViewSet):
    """
    A simple ViewSet for listing or retrieving comments.
    """
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
