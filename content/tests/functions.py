import json
from pathlib import Path

current_path = Path(__file__).parent


def get_api_posts_data():
    return True, json.loads(open(f'{current_path}/mock_data/posts.json').read())


def get_api_comments_data():
    return True, json.loads(open(f'{current_path}/mock_data/comments.json').read())
