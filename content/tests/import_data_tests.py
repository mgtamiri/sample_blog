from unittest.mock import patch

from rest_framework.test import APITestCase

from content.management.commands.importdata import Command as ImportCommand
from content.models import Post, Comment
from content.tests.functions import get_api_posts_data, get_api_comments_data


class ImportDataTest(APITestCase):

    @patch('commons.remote_api_connector.RemoteApiConnector.get_comments')
    @patch('commons.remote_api_connector.RemoteApiConnector.get_posts')
    def test_import_data_command(self, posts_mock, comments_mock):
        """
        ensure that import data process all data related to mock http 200 api call
        """

        posts_mock.return_value = get_api_posts_data()
        comments_mock.return_value = get_api_comments_data()

        command = ImportCommand()
        command.handle()

        self.assertEqual(Post.objects.all().count(), 100)
        self.assertEqual(Comment.objects.all().count(), 500)
