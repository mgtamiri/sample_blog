from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.utils.serializer_helpers import ReturnList

from content.serializers import PostSerializer


class CrudTest(APITestCase):

    def test_crud_post(self):
        # create post
        url_create = url_list = reverse('content:post-list')
        headers = {"Authorization": "bearer test"}
        data = {
            "title": "title",
            "body": "body",
            "user_id": 1
        }
        serializer = PostSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        response_create = self.client.post(url_create, data=serializer.validated_data, headers=headers).json()
        self.assertEqual(response_create.get("title"), data.get("title"))

        # list all posts
        response_list = self.client.get(url_list, format='json', headers=headers).json()
        serializer = PostSerializer(instance=response_list, many=True)
        self.assertEqual(type(serializer.data), ReturnList)

        url_update = url_detail = url_delete = reverse('content:post-detail', args=[response_create.get("id")])

        # detail created post
        response_detail = self.client.get(url_detail, format='json', headers=headers).json()
        serializer = PostSerializer(instance=response_detail)
        self.assertEqual(serializer.data.get("title"), data.get("title"))

        # update created post
        edited_data = {
            "title": "title_edited",
            "body": "body_edited",
            "user_id": 2
        }
        serializer = PostSerializer(data=edited_data)
        serializer.is_valid(raise_exception=True)
        response_update = self.client.put(url_update, data=serializer.validated_data, format='json',
                                          headers=headers).json()
        serializer = PostSerializer(instance=response_update)
        self.assertEqual(serializer.data.get("title"), edited_data.get("title"))

        # delete created post
        response_delete = self.client.delete(url_delete, format='json', headers=headers)
        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)
        response_detail = self.client.get(url_detail, format='json', headers=headers)
        self.assertEqual(response_detail.status_code, status.HTTP_404_NOT_FOUND)
