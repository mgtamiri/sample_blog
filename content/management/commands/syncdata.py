from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from commons.remote_api_connector import RemoteApiConnector
from content.models import SyncEvent


class Command(BaseCommand):
    help = 'sync data to fake API. as a master this command check all un applied events and send all to server  '

    def handle(self, *args, **options):
        """
        send all un applied events to fake API.
        """
        unapplied_events = SyncEvent.objects.filter(applied__isnull=True).order_by('id')
        remote_api_connector = RemoteApiConnector(settings.REMOTE_API_BASE_URL)

        # create a map to prevent use multi if conditions
        call_map = {
            (SyncEvent.OBJECT_POST, SyncEvent.EVENT_CREATE): remote_api_connector.create_post,
            (SyncEvent.OBJECT_POST, SyncEvent.EVENT_UPDATE): remote_api_connector.update_post,
            (SyncEvent.OBJECT_POST, SyncEvent.EVENT_DELETE): remote_api_connector.delete_post,
            (SyncEvent.OBJECT_COMMENT, SyncEvent.EVENT_CREATE): remote_api_connector.create_comment,
            (SyncEvent.OBJECT_COMMENT, SyncEvent.EVENT_UPDATE): remote_api_connector.update_comment,
            (SyncEvent.OBJECT_COMMENT, SyncEvent.EVENT_DELETE): remote_api_connector.delete_comment,

        }
        for unapplied_event in unapplied_events:
            try:
                params = {
                    "remote_id": unapplied_event.related_id,
                    "data": unapplied_event.data
                }

                res, data = call_map[(unapplied_event.object_type, unapplied_event.event_type)](**params)
                if res:
                    unapplied_event.applied = timezone.now()
                    unapplied_event.response = data

                    unapplied_event.save()
                    print(f"applying event {unapplied_event.id} successfully.")
                else:
                    print(f"applying event {unapplied_event.id} failed.")
                    return
            except Exception as e:
                print(f"error happened in applying event {unapplied_event.id}: {str(e)}")
                return
        print("Done... all events are now in sync.")
