from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import connections

from commons.remote_api_connector import RemoteApiConnector
from content.models import Post, Comment


class Command(BaseCommand):
    help = 'import sample data from fake api '

    @staticmethod
    def synchronize_last_sequence(model):
        """
        because of using bulk insert auto id field need to get update by max id in Postgres SQL.
        """
        sequence_name = f"{model._meta.db_table}_{model._meta.pk.name}_seq"
        with connections['default'].cursor() as cursor:
            cursor.execute(
                f"SELECT setval('{sequence_name}', (SELECT max({model._meta.pk.name}) FROM {model._meta.db_table}))"
            )

    def handle(self, *args, **options):
        """
        import posts and commons from a remote server
        """
        remote_api_connector = RemoteApiConnector(settings.REMOTE_API_BASE_URL)
        try:
            remote_posts_res, remote_posts = remote_api_connector.get_posts()
            posts = []

            for remote_post in remote_posts:
                posts.append(Post(id=remote_post.get('id'),
                                  title=remote_post.get('title'),
                                  body=remote_post.get('body'),
                                  user_id=remote_post.get('userId'),
                                  ))

            Post.objects.bulk_create(posts)
            self.synchronize_last_sequence(Post)
            print('posts imported successfully ... number of posts :', len(posts))
        except Exception as e:
            print('Error', str(e))

        try:
            remote_comments_res, remote_comments = remote_api_connector.get_comments()
            comments = []

            for remote_comment in remote_comments:
                comments.append(Comment(id=remote_comment.get('id'),
                                        name=remote_comment.get('name'),
                                        email=remote_comment.get('email'),
                                        body=remote_comment.get('body'),
                                        post_id=remote_comment.get('postId'),
                                        ))

            Comment.objects.bulk_create(comments)
            self.synchronize_last_sequence(Comment)
            print('comments imported successfully ... number of comments :', len(comments))
        except Exception as e:
            print('Error', str(e))
