from django.contrib import admin

from content.models import SyncEvent, Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'created_at']


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'email', 'created_at']


@admin.register(SyncEvent)
class SyncEventAdmin(admin.ModelAdmin):
    def is_applied(self, instance):
        return True if instance.applied else False

    is_applied.boolean = True
    list_display = ['id', 'event_type', 'object_type', 'related_id', 'is_applied']
