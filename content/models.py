from django.db import models
from django.utils.translation import gettext as _


class SyncEvent(models.Model):
    EVENT_CREATE = 1
    EVENT_UPDATE = 2
    EVENT_DELETE = 3
    EVENT_CHOICES = (
        (EVENT_CREATE, _("create")),
        (EVENT_UPDATE, _("update")),
        (EVENT_DELETE, _("delete")),
    )
    OBJECT_POST = 1
    OBJECT_COMMENT = 2

    OBJECT_CHOICES = (
        (OBJECT_POST, _("post")),
        (OBJECT_COMMENT, _("comment")),
    )
    event_type = models.PositiveSmallIntegerField(_("event type"), choices=EVENT_CHOICES)
    object_type = models.PositiveSmallIntegerField(_("object type"), choices=OBJECT_CHOICES)
    related_id = models.IntegerField(_("related object id"), null=True)
    data = models.JSONField(_("data in json"), null=True)
    response = models.JSONField(_("response in json"), null=True)
    applied = models.DateTimeField(_("apply date time"), null=True)

    class Meta:
        verbose_name = _('sync event')
        verbose_name_plural = _('sync events')

    def __str__(self):
        return str(self.id)


class SyncEventMixin:
    """
    this mixin will catch all save and delete actions and convert them to events for synchronization process
    """

    def save(self, bypass_sync=False, **kwargs):
        sync_related_id = self.id
        super(SyncEventMixin, self).save(**kwargs)
        if not bypass_sync:
            SyncEvent.objects.create(
                event_type=SyncEvent.EVENT_CREATE if not sync_related_id else SyncEvent.EVENT_UPDATE,
                object_type=self.sync_object_type,
                related_id=self.id,
                data=self.get_remote_data()
            )

    def delete(self, **kwargs):
        sync_related_id = self.id
        super(SyncEventMixin, self).delete(**kwargs)
        SyncEvent.objects.create(event_type=SyncEvent.EVENT_DELETE,
                                 object_type=self.sync_object_type,
                                 related_id=sync_related_id,
                                 )


class Post(SyncEventMixin, models.Model):
    # mixin will use this parameter to create event
    sync_object_type = SyncEvent.OBJECT_POST

    title = models.CharField(_("title"), max_length=250)
    body = models.TextField(_("body"))
    user_id = models.IntegerField(_("user id"))

    is_active = models.BooleanField(_('is active?'), default=True)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated at"), auto_now=True)

    def get_remote_data(self):
        """
        will provide a data for remote API
        """
        return {
            "id": self.id,
            "title": self.title,
            "body": self.body,
            "userId": self.user_id,
        }

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')

    def __str__(self):
        return self.title


class Comment(SyncEventMixin, models.Model):
    sync_object_type = SyncEvent.OBJECT_COMMENT

    name = models.CharField(_('name'), max_length=250)
    email = models.EmailField(_('email'))
    body = models.TextField(_('body'))
    post = models.ForeignKey(Post, verbose_name=_('post'), related_name='comments', on_delete=models.PROTECT)

    is_active = models.BooleanField(_('is active?'), default=True)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated at"), auto_now=True)

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')

    def get_remote_data(self):
        return {
            "id": self.id,
            "name": self.name,
            "body": self.body,
            "email": self.email,
            "post_id": self.post_id
        }

    def __str__(self):
        return self.name
