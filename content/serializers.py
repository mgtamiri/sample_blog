from rest_framework import serializers

from content.models import Post, Comment


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'title', 'body', 'user_id', 'created_at']
        read_only_fields = ['user_id', 'created_at']


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id', 'name', 'body', 'email', 'post', 'created_at']
        read_only_fields = ['created_at']
