from django.contrib import admin
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="Sample blog API",
        default_version='v1',
        description="a sample test project",
    ),
    public=True,
)
V1_API_PATH = 'api/v1/'
urlpatterns = [
    path('admin/', admin.site.urls),
    path(V1_API_PATH, include(('content.urls', 'content'), namespace='content')),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
