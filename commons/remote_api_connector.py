import requests


class RemoteApiConnector:
    """
    class to communicate to remote api.
    """
    def __init__(self, base_url):
        self.base_url = base_url

    ACTION_CREATE = "POST"
    ACTION_LIST = ACTION_RETRIEVE = "GET"
    ACTION_UPDATE = "PUT"
    ACTION_DELETE = "DELETE"

    def _send_request(self, endpoint, action, data=None):
        url = f'{self.base_url}/{endpoint}'
        response_data = requests.request(method=action, url=url, json=data).json()
        return True, response_data

    def get_posts(self):
        endpoint = 'posts'
        return self._send_request(endpoint, self.ACTION_LIST)

    def get_posts_by_id(self, remote_id, **kwargs):
        endpoint = f'posts/{remote_id}/'
        return self._send_request(endpoint, self.ACTION_RETRIEVE)

    def create_post(self, data, **kwargs):
        endpoint = f'posts/'
        return self._send_request(endpoint, self.ACTION_CREATE, data)

    def update_post(self, remote_id, data, **kwargs):
        endpoint = f'posts/{remote_id}/'
        return self._send_request(endpoint, self.ACTION_UPDATE, data)

    def delete_post(self, remote_id, **kwargs):
        endpoint = f'posts/{remote_id}/'
        return self._send_request(endpoint, self.ACTION_DELETE)

    def get_comments(self):
        endpoint = 'comments'
        return self._send_request(endpoint, self.ACTION_LIST)

    def get_comment_by_id(self, remote_id, **kwargs):
        endpoint = f'comments/{remote_id}/'
        return self._send_request(endpoint, self.ACTION_RETRIEVE)

    def create_comment(self, data, **kwargs):
        endpoint = f'comments/'
        return self._send_request(endpoint, self.ACTION_CREATE, data)

    def update_comment(self, remote_id, data, **kwargs):
        endpoint = f'comments/{remote_id}/'
        return self._send_request(endpoint, self.ACTION_UPDATE, data)

    def delete_comment(self, remote_id, **kwargs):
        endpoint = f'comments/{remote_id}/'
        return self._send_request(endpoint, self.ACTION_DELETE)
