from rest_framework.permissions import BasePermission


class FakeIsAuthenticated(BasePermission):
    """
    Fake Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user and isinstance(request.user, dict) and request.user.get('is_authenticated'))
